<?php
	get_header();
?> 
	
	<div class="container-wrap">
		<aside id="fh5co-hero">
			<div class="flexslider">
			<ul class="slides">
					<?php 
				    	$slider = new WP_Query(array(
				    		'post_type'		=> 'post',
				    		'category_name'	=> 'slider',
				    		'posts_per_page'=> 4,
				    		'order'			=> 'ASC',
				    	));
		   			?>
				<?php while($slider->have_posts())	: $slider->the_post();?>

					<li style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID,'ExternalUrl',true),'thumbnail'); ?>);">
                        <div class="overlay-gradient"></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-md-pull-3 slider-text">
                                    <div class="slider-text-inner">
                                        <h1><?php the_title(); ?></h1>
                                         <h2><?php the_content(); ?></h2>
                                         
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                <?php  endwhile; ?>
                   <!-- <li style="background-image: url(images/img_bg_2.jpg);">
                        <div class="overlay-gradient"></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-md-push-3 slider-text">
                                    <div class="slider-text-inner">
                                        <h1>WordPress Theme For People Who Tell Stories</h1>
                                         <h2>Free html5 templates Made by <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></h2>
                                         <p><a class="btn btn-primary btn-demo" href="#"></i> View Demo</a> <a class="btn btn-primary btn-learn">Learn More</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="background-image: url(images/img_bg_3.jpg);">
                        <div class="overlay-gradient"></div>
                        <div class="container-fluids">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 slider-text">
                                    <div class="slider-text-inner text-center">
                                        <h1>What Would You Like To Learn?</h1>
                                         <h2>Free html5 templates Made by <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></h2>
                                         <p><a class="btn btn-primary btn-demo" href="#"></i> View Demo</a> <a class="btn btn-primary btn-learn">Learn More</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="background-image: url(images/img_bg_4.jpg);">
                        <div class="overlay-gradient"></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-md-push-3 slider-text">
                                    <div class="slider-text-inner">
                                        <h1>I Love to Tell My Story</h1>
                                         <h2>Free html5 templates Made by <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></h2>
                                         <p><a class="btn btn-primary btn-demo" href="#"></i> View Demo</a> <a class="btn btn-primary btn-learn">Learn More</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li> -->
			  	</ul>
		  	</div>
		</aside>
		<div id="fh5co-services">
			<div class="row">
				<?php 
					$servpost = new WP_Query(array(
						'post_type'		=>'post',
						'category_name'	=>'services',
						'order'			=>'ASC',
						'post_per_page'	=> 3,
					));		
				?>	
					<?php while($servpost->have_posts()) : $servpost->the_post(); ?>
				<div class="col-md-4 text-center animate-box">
				
				
							<h3><a href="<?php the_permalink(); ?>"></a></h3>
							<p><?php the_content(); ?></p>
					
							
				</div>
				<?php endwhile; ?>
				<!--	<div class="col-md-4 text-center animate-box">
                        <div class="services">
                            <span class="icon">
                                <i class="icon-lab2"></i>
                            </span>
                            <div class="desc">
                                <h3><a href="#">Web Design &amp; UI</a></h3>
                                <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center animate-box">
                        <div class="services">
                            <span class="icon">
                                <i class="icon-settings"></i>
                            </span>
                            <div class="desc">
                                <h3><a href="#">Web Development</a></h3>
                                <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
                            </div>
                        </div>
                    </div> -->
			</div>
		</div> 

		<div id="fh5co-work" class="fh5co-light-grey">
			<div class="row animate-box">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
					<h2>Work</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">
				<?php
					$animate = new WP_Query(array(
						'post_type'	=>	'post',
						'post_per_page'	=>	3,
						'category_name'	=>	'animate',
						'order'	=>	'ASC',
					));
				?>

				<?php while($animate->have_posts())	: 	$animate->the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
				<!--<div class="col-md-4 text-center animate-box">
					<a href="work-single.html" class="work" style="background-image: url(images/portfolio-2.jpg);">
						<div class="desc">
							<h3>Project Name</h3>
							<span>Brading</span>
						</div>
					</a>
				</div>
				<div class="col-md-4 text-center animate-box">
					<a href="work-single.html" class="work" style="background-image: url(images/portfolio-3.jpg);">
						<div class="desc">
							<h3>Project Name</h3>
							<span>Illustration</span>
						</div>
					</a>
				</div>-->
			</div>
		</div>

		

		<div id="fh5co-blog" class="blog-flex">
			<?php 
				$query = new WP_Query(array(
					'post_type' => 'post',
					'category_name' => 'front-a',
					'order' => 'ASC',
					'posts_per_page' => 1,
				));

			?>


	<?php while($query->have_posts()) : $query->the_post(); ?>
			<div class="featured-blog"  style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID,'ExternalUrl',true),'thumbnail'); ?> ">
				<div class="desc-t">
					<div class="desc-tc">
						<?php the_content(); ?>

						<!-- <span class="featured-head">Featured Posts</span>
						<h3><a href="#">Top 20 Best WordPress Themes 2017 Multi Purpose and Creative Websites</a></h3>
						<span><a href="#" class="read-button">Learn More</a></span> -->
					</div>
				</div>
			</div>
	<?php endwhile; ?>

			<div class="blog-entry fh5co-light-grey">
				<div class="row animate-box">
					<div class="col-md-12">
						<h2>Latest Posts</h2>
					</div>
				</div>

				<div class="row">	

					<?php
						$animate_side = new WP_Query(array(
							'post_type'	=>	'post',
							'post_per_page'	=>	3,
							'order'	=>	'ASC',
							'category_name'	=>	'animate_side',
						));
					?>

				<?php while ($animate_side->have_posts())	:$animate_side->the_post();  ?>


					<div class="col-md-12 animate-box">
						<a href="#" class="blog-post">
							<span class="img" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID,'ExternalUrl',true),'thumbnail'); ?>);"></span>
							<div class="desc">
								<h3><?php the_title(); ?></h3>
								<span class="cat"><?php the_content(); ?></span>
							</div>
						</a>
					</div>
				<?php endwhile; ?>
					<!--<div class="col-md-12 animate-box">
						<a href="#" class="blog-post">
							<span class="img" style="background-image: url(images/blog-1.jpg);"></span>
							<div class="desc">
								<h3>16 Outstanding Photography WordPress Themes You Must See</h3>
								<span class="cat">Collection</span>
							</div>
						</a>
					</div>
					<div class="col-md-12 animate-box">
						<a href="#" class="blog-post">
							<span class="img" style="background-image: url(images/blog-3.jpg);"></span>
							<div class="desc">
								<h3>16 Outstanding Photography WordPress Themes You Must See</h3>
								<span class="cat">Collection</span>
							</div>
						</a>
					</div>-->
				</div>
			</div>
		</div>
	</div><!-- END container-wrap -->
<?php
	get_footer();
?>
	