<?php
 get_header();
?>
<div class="error">
	<div class="container">
		
		<div class="wrap-about py-5 ftco-animate">
			<div class="heading-section mb-5">
				<h1 class="mb_4 text-center">404 NOT FOUND!</h1>
			</div>
			<div class="">
				<p class="text-justify text-center">
					Maybe you're looking for something else!<br/>
					Please visit <a href="<?php bloginfo('home'); ?>">home page</a>. 
				</p>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>