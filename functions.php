<?php
function neat_function(){

	//adding navwalker file
	require_once get_template_directory(). '/wp-bootstrap-navwalker.php';

	//custom title
	add_theme_support('title_tag');

	//thumbnail
	add_theme_support('post-thumbnails');

	//custom background
	add_theme_support('custom-background');

	//custom logo
	add_theme_support('custom-logo',array('defualt-image'	=>	get_template_directory_uri().'/images/logo.png',
	));

	//load theme textdomain
	load_theme_textdomain('neat',get_template_directory_uri().'/languages');


	//register menu
	if(function_exists('register_nav_menus')){
	register_nav_menus(array(
	'primarymenu' 		=> __('Header Menu','neat'),
	'secondarymenu'		=> __('Footer Menu','neat')
		));
	}

	//read more
	function read_more($limit){
		$post_content = explode(" ", get_the_content());

		$less_content = array_slice($post_content,0,$limit);
		echo implode(" ",$less_content);
	}
	
}

add_action('after_setup_theme','neat_function');


function neat_widgets(){

	register_sidebar(array(
		'name'	=>	__('Footer Widgets','neat'),
		'description'	=>	__('Add your footer widgets here','neat'),
		'id'	=>	'footer-widgets',
		'before_widget'	=>	'<div class="col-md-3 fh5co-widget">',
		'after_widget'	=>	'</p></div>',
		'before_title'	=>	'<h4>',
		'after_title'	=>	'</h4><p>',
	
	));

	register_sidebar(array(
		'name'	=>	__('Copyright Widgets','neat'),
		'description'	=>	__('Add your Copyright widgets here','neat'),
		'id'	=>	'copyright-widgets',
		'before_widget'	=>	'<div class="row copyright"',
		'after_widget'	=>	'</p></div>',
		'before_title'	=>	'',
		'after_title'	=>	'<p>',
	
	));
	
}
add_action('widgets_init','neat_widgets');

?>