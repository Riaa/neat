<!DOCTYPE HTML>
<html<?php language_attributes(); ?>>
	<head>


	<meta charset="<?php bloginfo('charset');?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="viewport" http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Neat &mdash; Free Website Template, Free HTML5 Template by freehtml5.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="freehtml5.co" />

	

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Oxygen:300,400" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/magnific-popup.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/flexslider.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/style2.css">
	<link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo esc_url(get_template_directory_uri());?> /js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container-wrap">
			<div class="top-menu">
				<div class="row">
					<div class="col-xs-2">
						<div id="fh5co-logo">
							<?php
								if (function_exists('the_custom_logo')) {
									the_custom_logo();
								}
								
							?>
						</div>
					</div>
					<?php
							wp_nav_menu(array(
								'theme_location'		=>	'primarymenu',
								'depth'					=>	'2',
								'container'				=>	'div',
								'container_class'		=>	'col-xs-10 text-right menu-1',
								'container_id'			=>	'',
								'menu_class'			=>	'',
								'fallback_cb'			=>	'wp_Bootstrap_Navwalker::fallback',
								'walker'				=>	new WP_Bootstrap_Navwalker(),
							));
						?>
					<!--<div class="col-xs-10 text-right menu-1">
						<ul>
							<li class="active"><a href="index.php">Home</a></li>
							<li><a href="work.php">Work</a></li>
							<li class="has-dropdown">
								<a href="blog.php">Blog</a>
								<ul class="dropdown">
									<li><a href="#">Web Design</a></li>
									<li><a href="#">eCommerce</a></li>
									<li><a href="#">Branding</a></li>
									<li><a href="#">API</a></li>
								</ul>
							</li>
							<li><a href="about.php">About</a></li>
							<li><a href="contact.php">Contact</a></li>
						</ul>
					</div>
					<div class="col-xs-9 text-right menu-1">
						<?php
							/*wp_nav_menu(array(
								'theme_location'		=>	'primarymenu',
								'depth'					=>	'2',
								'container-wrap'		=>	'div',
								'container-wrap_class'	=>	'',
								'container-wrap_id'		=>	'',
								'menu_class'			=>	'',
								'fallback_cb'			=>	'wp_Bootstrap_Navwalker::fallback',
								'walker'				=>	new WP_Bootstrap_Navwalker(),
							)); */
						?>
						<ul>
							<li class="active"><a href="index.php">Home</a></li>
							<li><a href="work.php">Work</a></li>
							<li class="has-dropdown">
								<a href="blog.php">Blog</a>
								<ul class="dropdown">
									<li><a href="#">Web Design</a></li>
									<li><a href="#">eCommerce</a></li>
									<li><a href="#">Branding</a></li>
									<li><a href="#">API</a></li>
								</ul>
							</li>
							<li><a href="about.php">About</a></li>
							<li><a href="contact.php">Contact</a></li>
						</ul>
					</div>-->
				</div>
				
			</div>
		</div>
	</nav>